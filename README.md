# Dataset: "Mixed Reality Deictic Gesture for Multi-Modal Robot Communication"

## Paper

http://mirrorlab.mines.edu/pubs/?p=williams2019hri

## Citation

```
@inproceedings{williams2019hri,
author={Tom Williams and Matthew Bussing and Sebastian Cabroll and Elizabeth Boyle and Nhan Tran},
title={Mixed Reality Deictic Gesture for Multi-Modal Robot Communication}
booktitle={Proceedings of the 14th ACM/IEEE International Conference on Human-Robot Interaction (HRI)},
year={2019}
}
```

## Format

Each row represents one user.
- id column: anonymized user ID
- accuracy columns: did the user click on the correct object?
- effectiveness: what was the perceived effectiveness?
- likeability: what was the perceived likeability? (averaged Godspeed II scores: contact the authors for raw scores)
- reaction time: how long did it take to click on the object (MS)?
 
The column header format is in the format iijk, where
- ii: Communication Style
  - 01: AR visualization used with bare NP ([circle] "that block")
  - 10: complex NP used ("that red block")
  - 11: AR + complex NP used ("[circle] that red block")
- j: Inherent Ambiguity
  - 0: unambiguous referent
  - 1: unambiguous referent
- k: Distance
  - 0: referent near to robot
  - 1: referent far from robot

